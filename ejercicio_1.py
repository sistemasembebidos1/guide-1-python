from math import pi, sqrt, pow

r=float(input("Ingrese el radio: "))
a=float(input("Ingrese la altura: "))

s=2*pi*r+sqrt(pow(a,2)+pow(4,2))

print("El area de la superficie lateral de un cono recto dado los parametros ingresados es:", s)

